package edu.uoc.android.currentweek;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    public static final String EXTRA_VALUE = "edu.uoc.android.currentweek.VALUECURRENTWEEKNUMBER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /** Method called when the user touches de button **/
    public void checkValueCurrentWeek(View view){
        /** Get the text view from layout **/
        final TextView input = findViewById(R.id.editText);
        if (input.getText().length()==0) {
            /** The user didn't introduce a value in the input text field **/
            input.setError(getString(R.string.main_error));
        } else {
            /** Create an intent to go to ResultsActivity **/
            Intent intent = new Intent(this, ResultActivity.class);
            /** Get the value from the input text field **/
            String valueCurrentWeekNumber = input.getText().toString();
            /** Add the value to the intent **/
            intent.putExtra(EXTRA_VALUE, valueCurrentWeekNumber);
            /** Start the ResultsActivity **/
            startActivity(intent);
        }
    }
}
